import os
import sqlite3
import random
import time
import ujson
import requests

from flask import Blueprint, render_template, redirect, request, Response, send_from_directory, session
from werkzeug.utils import secure_filename
from passlib.hash import sha256_crypt
from datetime import datetime, timedelta

from decorators import require_login

import utils
import logger

blueprint = Blueprint('user', __name__, url_prefix='/api/user')
config = utils.get_config().get('app', {})

@blueprint.route('/', methods=['GET'])
@require_login
def get_user():
    with sqlite3.connect('./data/database.db') as c:
        user = c.execute('SELECT id, email, name, avatar, access FROM users WHERE id = ?', (session.get('id'),)).fetchone()

    if not user:
        return '', 404

    json = {'id':user[0], 'email':user[1], 'name':user[2], 'avatar':user[3], 'access':[x.strip() for x in user[4].split(',')]}

    return Response(ujson.dumps(json), content_type='application/json')


@blueprint.route('/profile/<string:username>', methods=['GET'])
def get_profile(username):
    with sqlite3.connect('./data/database.db') as c:
        user = c.execute('SELECT email, name, avatar FROM users WHERE name = ?', (username,)).fetchone()

    if not user:
        return '', 400

    json = {'email':user[0], 'name':user[1], 'avatar':user[2]}

    return Response(ujson.dumps(json), content_type='application/json')


@blueprint.route('/update', methods=['POST'])
@require_login
def update_userinfo():
    data = request.get_json() or request.form
    new_email = data['email'].strip()
    new_name = data['name'].strip()
    # new_phone = str(data['phone']).strip()

    if not all([new_email and '@' in new_email, new_name]):
        return '', 400

    with sqlite3.connect('./data/database.db') as c:
        access = c.execute('SELECT access FROM users WHERE id = ?', (session.get('id'),)).fetchone()[0]

        if new_email is not session.get('email'):
            if 'email_unverified' not in access:
                c.execute('UPDATE users SET access = ? WHERE id = ?', (access+',email_unverified', session.get('id'),))

        # if new_phone is not session.get('phone'):
        #     if 'sms_unverified' not in access:
        #         c.execute('UPDATE users SET access = ? WHERE id = ?', (access+',sms_unverified', session.get('id'),))

        c.execute('UPDATE users SET email = ?, name = ? WHERE id = ?', (new_email, new_name, session.get('id')))

    utils.reload_session(session.get('id'))

    return '', 200


@blueprint.route('/update/avatar', methods=['POST'])
@require_login
def update_avatar():
    file = request.files['file']

    if not file:
        return 'No file sent', 400

    filename = secure_filename(file.filename)

    if not utils.is_image(filename):
        return 'No image sent', 400

    new_filename = utils.random_name(12) + '.' + utils.get_ext(filename)
    save_dir = './static/uploads/avatars'

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    file.save(os.path.join(save_dir, new_filename))
    os.chmod(os.path.join(save_dir, new_filename), 0o755)

    with sqlite3.connect('./data/database.db') as c:
        c.execute('UPDATE users SET avatar = ? WHERE id = ?', ('/user/static/uploads/avatars/'+new_filename, session.get('id')))

    print(session.get('avatar'))
    
    if not session.get('avatar') == 'default_avatar.png':
        try:
            os.remove(os.path.join(save_dir, session.get('avatar')))
        except OSError:
            pass

    utils.reload_session(session.get('id'))

    return '', 200


@blueprint.route('/update/password', methods=['POST'])
@require_login
def update_password():
    data = request.get_json() or request.form
    old_pw = data['old_password'].strip()
    new_pw = data['new_password'].strip()

    if not all([old_pw, new_pw]):
        return '', 400

    with sqlite3.connect('./data/database.db') as c:
        old_pw_crypt = c.execute('SELECT password FROM users WHERE id = ?', (session.get('id'),)).fetchone()[0]

        if sha256_crypt.verify(old_pw, old_pw_crypt):
            pw_crypt = sha256_crypt.encrypt(new_pw)
            c.execute('UPDATE users SET password = ? WHERE id = ?', (pw_crypt, session.get('id')))

            return '', 200

    return '', 400


@blueprint.route('/verify/<string:kind>', methods=['POST'])
@require_login
def verify(kind):
    if kind not in ('sms', 'email') or kind+'_unverified' not in session.get('access'):
        return '', 400

    code = random.randint(100000, 999999)

    with sqlite3.connect('./data/database.db') as c:
        # delete any previous codes for user & kind
        c.execute('DELETE FROM verifications WHERE user = ? AND type = ?', (session.get('id'), kind,))

        # insert new
        c.execute('INSERT INTO verifications VALUES (NULL, ?, ?, ?, ?)', (session.get('id'), int(time.time()), kind, code,))

    logger.log('%s VERIFICATION GENERATED: %d' % (kind.upper(), code,), userid=session.get('id'))

    if kind == 'sms':
        utils.send_sms('+'+str(session.get('phone')),
                       render_template('sms/new_user_pin.txt', code=code))
        logger.log('SMS VERIFY SENT: +%d' % (session.get('phone'),), userid=session.get('id'), level='W')

    if kind == 'email':
        utils.send_mail(session.get('email'),
                        'Email Verfication',
                        render_template('email/verify_email.html', code=code))
        logger.log('EMAIL VERIFY SENT: '+session.get('email'), userid=session.get('id'), level='W')

    return '', 200


@blueprint.route('/verify/<string:kind>/confirm', methods=['POST'])
@require_login
def verify_confirm(kind):
    code = request.form.get('code')
    if kind not in ('sms', 'email') or not (code and 1000 <= int(code) <= 999999):
        return '', 400

    with sqlite3.connect('./data/database.db') as c:
        # delete verifications over an hour old
        c.execute('DELETE FROM verifications WHERE timestamp < ?', (int(time.time()) - 3600,))

        # check matches
        if c.execute('SELECT COUNT(code) FROM verifications WHERE user = ? AND type = ? AND code = ?',
                     (session.get('id'), kind, code,)).fetchone()[0] == 0:
            return '', 401

        # match found, delete
        c.execute('DELETE FROM verifications WHERE user = ? AND type = ? AND code = ?',
                  (session.get('id'), kind, code,))

    # remove unverified status
    session.get('access').remove(kind+'_unverified')
    print(session.get('access')[0], session.get('id'))
    utils.update_access(session.get('id'), session.get('access')[0])

    logger.log('%s VERIFIED' % (kind.upper(),), userid=session.get('id'))

    return '', 200


@blueprint.before_request
def before_request():
    # user must be logged in
    if not session.get('id'):
        return

    with sqlite3.connect('./data/database.db') as c:
        actions = utils.clear_session_actions(session.get('id'), retrieve=True)

    # need something to do
    if not actions:
        return

    # sorting puts force_logout first for now
    actions = sorted(actions)

    for action in actions:
        if action == 'force_logout':
            logger.log('APPLYING FORCE_LOGOUT', userid=session.get('id'), level='W')
            for key in session.copy().keys():
                del session[key]
            session.clear()

            # nothing else should matter, since relogin will cause reloading access, etc.
            return

        if action.startswith('reload_session'):
            with sqlite3.connect('./data/database.db') as c:
                row = c.execute('SELECT id, email, password, name, phone, access, prefs FROM users WHERE id = ?', (session.get('id'),)).fetchone()

            utils.login(row)

            logger.log('LIVE SESSION RELOADED', userid=session.get('id'), level='W')


    # invalid session for emergency forced-logouts
    if session.get('email') and session.get('force_logout'):

        return redirect('/login')
