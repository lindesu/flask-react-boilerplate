import os
import base64
import sqlite3
import ujson

from datetime import datetime
from flask import Blueprint, render_template, redirect, request, Response, send_from_directory, session
from passlib.hash import sha256_crypt
from random import *

import utils
import logger

blueprint = Blueprint('auth', __name__, url_prefix='/api/auth')


@blueprint.route('/register', methods=['POST'])
def register():
    data = request.get_json() or request.form
    email = data['email'].strip()
    name = data['name'].strip()
    # phone = data['phone'].strip()

    if not all([email and '@' in email, name]):
        return '', 400

    pw = base64.b32encode(os.urandom(10))[8:].upper()
    pw_crypt = sha256_crypt.encrypt(pw)

    with sqlite3.connect('./data/database.db') as c:
        c.execute('INSERT INTO users VALUES (NULL, ?, ?, ?, ?, NULL, ?, ?)', (email, pw_crypt, name, '/user/static/default_avatar.png', 'email_unverified, sms_unverified', '{}'))
        newid = c.execute('SELECT id FROM users WHERE email = ?', (email,)).fetchone()[0]

    # logger.log('USER CREATED: %d:%s' % (newid, email,), userid=session.get('id', 0), level='I')
    utils.send_mail(email,
                    'Welcome',
                    render_template('email/new_user.html',
                                    email=email,
                                    name=name,
                                    pw=pw.decode("utf-8")))

    return '', 200


@blueprint.route('/login', methods=['POST'])
def login():
    data = request.get_json() or request.form
    email = data['email'].strip()
    pw = data['password'].strip()

    if not (email and pw):
        logger.log('LOGIN FAILURE: %s:%s' % (email, pw,), userid=0)
        return '', 400

    try:
        row = []
        with sqlite3.connect('./data/database.db') as c:
            row = c.execute('SELECT id, email, password, name, avatar, phone, access, prefs FROM users WHERE email = ?', (email,)).fetchone()

            if sha256_crypt.verify(pw, row[2]):
                utils.login(row)

                return '', 200

    except Exception as e:
        logger.log('LOGIN FAILURE: %s:%s' % (email, e,), userid=0)

    return '', 401


@blueprint.route('/logout', methods=['GET'])
def logout():
    logger.log('LOGOUT', userid=session.get('id'))

    utils.clear_session_actions(session.get('id'))

    for key in session.copy().keys():
        del session[key]

    session.clear()

    return '', 200
