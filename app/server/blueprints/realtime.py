import os
import sqlite3
import random
import time
import ujson
import requests

from flask import Blueprint, g, render_template, redirect, request, Response, send_from_directory, session
from werkzeug.utils import secure_filename
from passlib.hash import sha256_crypt
from datetime import datetime, timedelta

import rethinkdb as r
from rethinkdb import RqlRuntimeError

from decorators import require_login

import utils
import logger

blueprint = Blueprint('realtime', __name__, url_prefix='/api/realtime')
config = utils.get_config().get('app', {})


@blueprint.route('/', methods=['GET'])
@require_login
def get_feed():
    json = list(r.table("chats").order_by(index=r.desc('created')).limit(20).run(g.db_conn))

    return Response(ujson.dumps(json), content_type='application/json')


@blueprint.route('/create_message', methods=['POST'])
@require_login
def create_message():
    data = request.get_json() or request.form
    data['created'] = datetime.now(r.make_timezone('00:00'))
    name = data['name'].strip()
    message = data['message'].strip()

    if not all([name, message]):
        return '', 400
    
    new_chat = r.table("chats").insert([ data ]).run(g.db_conn)
        
    return '', 200


@blueprint.before_request
def before_request():
    # user must be logged in
    if not session.get('id'):
        return

    try:
        g.db_conn = r.connect(host='rethinkdb', 
                              port=28015, 
                              db='chat')
    except RqlDriverError:
        abort(503, "No database connection could be established.")


@blueprint.teardown_request
def teardown_request(exception):
    try:
        g.db_conn.close()
    except AttributeError:
        pass
