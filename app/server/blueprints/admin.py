import base64
import importlib
import json
import ujson
import os
import random
import sqlite3
import sys
import time

from flask import Blueprint, Flask, redirect, render_template, request, Response, session
from passlib.hash import sha256_crypt
from datetime import datetime

import logger
import utils

from decorators import require_access

blueprint = Blueprint('admin', __name__, url_prefix='/api/admin')
config = utils.get_config()


@blueprint.route('/contact/submit', methods=['POST'])
def contact_submit():
    data = request.get_json() or request.form
    email = data['email'].strip()
    name = data['name'].strip()
    phone = data['phone'].strip()
    message = data['message'].strip()

    if not all([email and '@' in email, name, phone, message]):
        return '', 400

    utils.send_mail(config.get('mail').get('contact'),
                    'Boilerplate',
                    render_template('email/contact.html',
                                    phone=phone,
                                    email=email,
                                    name=name,
                                    message=message))

    return '', 200


@blueprint.route('/logs')
@require_access('admin')
def admin_logs_page():
    with sqlite3.connect('./data/database.db') as c:
        logs = c.execute('SELECT timestamp, level, user, message, users.email, users.name FROM logs JOIN users ON user = users.id ORDER BY timestamp DESC limit 500').fetchall()

    for i in range(0, len(logs)):
        logs[i] = list(logs[i])
        logs[i][0] = datetime.fromtimestamp(float(logs[i][0])).strftime('%Y/%m/%d %H:%M:%S')

    json = {'logs':logs,}

    return Response(ujson.dumps(json), content_type='application/json')


@blueprint.route('/users')
@require_access('admin')
def admin_users_page():
    with sqlite3.connect('./data/database.db') as c:
        users = c.execute("SELECT id, email, avatar, name, phone, access, prefs, password = '' FROM users ORDER BY name, email").fetchall()

    json = {'users':users,}

    return Response(ujson.dumps(json), content_type='application/json')


@blueprint.route('/user/create', methods=['POST'])
@require_access('admin')
def user_create():
    data = request.get_json() or request.form
    email = data['email'].strip()
    name = data['name'].strip()
    timestamp = datetime.utcnow()

    if not all([email and '@' in email, name]):
        return '', 400

    pw = base64.b32encode(os.urandom(10))[8:].upper()
    pw_crypt = sha256_crypt.encrypt(pw)

    with sqlite3.connect('./data/database.db') as c:
        # if data['test']:

        c.execute('INSERT INTO users VALUES (NULL, ?, ?, ?, ?, NULL, ?, ?)', (email, pw_crypt, name, '/user/static/default_avatar.png', 'email_unverified, sms_unverified', '{}'))
        newid = c.execute('SELECT id FROM users WHERE email = ?', (email,)).fetchone()[0]

    logger.log('USER CREATED: %d:%s' % (newid, email,), userid=session.get('id', 0), level='I')

    utils.send_mail(email,
                    'Welcome',
                    render_template('email/new_user.html',
                                    email=email,
                                    name=name,
                                    pw=pw.decode("utf-8")))

    return '', 200


@blueprint.route('/user/access', methods=['POST'])
@require_access('admin')
def admin_user_edit_access():
    data = request.get_json() or request.form
    access = ','.join([x.strip() for x in data['access'].split(',')])
    uid = data['id']

    with sqlite3.connect('./data/database.db') as c:
        c.execute('UPDATE users SET access = ? WHERE id = ?', (access, uid,))

    logger.log('UPDATED ACCESS FOR ID %s: %s' % (uid, access,), userid=session.get('id'), level='W')

    utils.reload_session(uid)

    return '', 200


@blueprint.route('/user/update', methods=['POST'])
@require_access('admin')
def admin_update_user_info():
    data = request.get_json() or request.form
    new_email = data['email'].strip()
    new_name = data['name'].strip()
    uid = data['id']
    # new_phone = str(data['phone']).strip()

    if not all([new_email and '@' in new_email, new_name]):
        return '', 400

    with sqlite3.connect('./data/database.db') as c:
        email, access = c.execute('SELECT email, access FROM users WHERE id = ?', (uid,)).fetchone()

        if new_email is not email:
            if 'email_unverified' not in access:
                c.execute('UPDATE users SET access = ? WHERE id = ?', (access+',email_unverified', uid,))

        # if new_phone is not session.get('phone'):
        #     if 'sms_unverified' not in access:
        #         c.execute('UPDATE users SET access = ? WHERE id = ?', (access+',sms_unverified', uid,))

        c.execute('UPDATE users SET email = ?, name = ? WHERE id = ?', (new_email, new_name, uid,))

    utils.force_logout(uid)

    return '', 200


@blueprint.route('/user/name', methods=['POST'])
@require_access('admin')
def admin_user_edit_name():
    data = request.get_json() or request.form
    uid = data['id']
    name = data['name'].strip()

    with sqlite3.connect('./data/database.db') as c:
        c.execute('UPDATE users SET name = ? WHERE id = ?', (name, uid,))

    logger.log('UPDATED NAME FOR ID %s: %s' % (uid, name,), userid=session.get('id'), level='W')

    return '', 200


@blueprint.route('/user/email', methods=['POST'])
@require_access('admin')
def admin_user_edit_email():
    data = request.get_json() or request.form
    uid = data['id']
    email = data['email'].strip()

    with sqlite3.connect('./data/database.db') as c:
        c.execute('UPDATE users SET email = ? WHERE id = ?', (email, uid,))
        access = c.execute('SELECT access FROM users WHERE id = ?', (uid,)).fetchone()[0]
        if 'email_unverified' not in access:
            c.execute('UPDATE users SET access = ? WHERE id = ?', (access+',email_unverified', uid,))

    logger.log('CHANGED EMAIL FOR ID %s: %s' % (uid, email,), userid=session.get('id'), level='W')

    # force a logout, if there's an email collision problem or something, deal with it asap
    utils.force_logout(uid)

    return '', 200


@blueprint.route('/user/phone', methods=['POST'])
@require_access('admin')
def admin_user_edit_phone():
    data = request.get_json() or request.form
    uid = data['id'].strip()
    phone = data['phone'].strip()

    with sqlite3.connect('./data/database.db') as c:
        c.execute('UPDATE users SET phone = ? WHERE id = ?', (phone, uid,))
        access = c.execute('SELECT access FROM users WHERE id = ?', (uid,)).fetchone()[0]
        if 'sms_unverified' not in access:
            c.execute('UPDATE users SET access = ? WHERE id = ?', (access+',sms_unverified', uid,))

    logger.log('UPDATED PHONE FOR ID %s: %s' % (uid, phone,), userid=session.get('id'), level='W')

    utils.reload_session(uid)

    return '', 200


@blueprint.route('/user/disable', methods=['POST'])
@require_access('admin')
def admin_user_disable():
    data = request.get_json() or request.form
    uid = data['id']

    with sqlite3.connect('./data/database.db') as c:
        c.execute("UPDATE users SET password = '' WHERE id = ?", (uid,))

    logger.log('USER DISABLED %s' % (uid,), userid=session.get('id'), level='W')

    utils.force_logout(uid)

    return '', 200


@blueprint.route('/user/resetpw', methods=['POST'])
@require_access('admin')
def admin_user_reset_password():
    data = request.get_json() or request.form
    uid = data['id']

    pw = base64.b32encode(os.urandom(10))[8:].upper()
    pw_crypt = sha256_crypt.encrypt(pw)

    with sqlite3.connect('./data/database.db') as c:
        email, name = c.execute("SELECT email, name FROM users WHERE id = ?", (uid,)).fetchone()
        c.execute('UPDATE users SET password = ? WHERE id = ?', (pw_crypt, uid,))

    logger.log('PASSWORD RESET FOR %s' % (uid,), userid=session.get('id'), level='W')

    utils.send_mail(email,
                    'Password Reset!',
                    render_template('email/password_reset.html',
                                    email=email,
                                    name=name,
                                    pw=pw.decode("utf-8")))

    utils.force_logout(uid)

    return '', 200
