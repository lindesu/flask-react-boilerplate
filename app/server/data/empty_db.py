import sqlite3

items = 0

with sqlite3.connect('database.db') as c:
    for table in c.execute("SELECT * FROM sqlite_master WHERE type='table';").fetchall():
        c.execute("DELETE FROM %s" % table[1])
        items += 1

print("** CLEARED %d TABLES" % (items,))
