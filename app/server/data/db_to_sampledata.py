import sqlite3

items = 0

with sqlite3.connect('database.db') as c:
    with open('sampledata.sql', 'w+') as f:
        for line in c.iterdump():
            f.write('%s\n' % line)
            items += 1

print("** DUMPED %d LINES" % (items,))