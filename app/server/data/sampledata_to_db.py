import sqlite3
import os

if os.path.isfile('database.db'):
    os.remove('database.db')

with open('sampledata.sql', 'r') as f:
    with sqlite3.connect('database.db') as c:
        c.executescript(f.read())

print("** CREATED DB FROM DUMP")
