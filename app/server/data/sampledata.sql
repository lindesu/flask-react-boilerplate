BEGIN TRANSACTION;
CREATE TABLE "logs" (

	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,

	`timestamp`	NUMERIC,

	`level`	TEXT,

	`user`	NUMERIC,

	`message`	TEXT

);
CREATE TABLE `sessions` (
	`user`	INTEGER,
	`timestamp`	INTEGER,
	`flag`	TEXT
);
DELETE FROM "sqlite_sequence";
CREATE TABLE "users" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`email`	TEXT UNIQUE,
	`password`	INTEGER,
	`name`	TEXT,
	`avatar`	TEXT,
	`phone`	TEXT,
	`access`	TEXT,
	`prefs`	TEXT
);
INSERT INTO "users" VALUES(1,'dev@gmail.com','$5$rounds=535000$b.RZD3BZwsPSBq3E$evqYQdwKyT5sk0KWgFz6GMyTbtu5eq8y/IOwBWDVtQD','Admin','/user/static/default_avatar.png','2452452245','admin','{}');
CREATE TABLE `verifications` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`user`	INTEGER,
	`timestamp`	INTEGER,
	`type`	TEXT,
	`code`	TEXT
);
COMMIT;
