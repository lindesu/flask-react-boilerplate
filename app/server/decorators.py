from functools import wraps
import sqlite3

from flask import redirect
from flask import request
from flask import session
from threading import Thread


# def api_required(func):
#     @wraps(func)
#     def decorated_view(*args, **kwargs):
#         apikey = request.form.get('apikey') or request.headers.get('Esix-API-Key')
#         if not apikey:
#             return '', 401
#
#         with sqlite3.connect('data/users.db') as db:
#             c = db.cursor()
#             authed = c.execute('SELECT COUNT(*) FROM users WHERE apikey = ?', (apikey,)).fetchone()[0] == 1
#
#         if not authed:
#             return '', 401
#
#         return func(*args, **kwargs)
#     return decorated_view


def require_access(access):
    def args_decorator(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if not session.get('email'):
                return '', 401

            if access in session.get('access', []):
                return func(*args, **kwargs)

            return '', 401

        return decorated_view
    return args_decorator


def require_login(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not session.get('email'):
            return '', 401

        return func(*args, **kwargs)

    return decorated_view


def async(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper

#
# def local_required(func):
#     @wraps(func)
#     def decorated_view(*args, **kwargs):
#         if request.headers not in (current_app.config.get('local_ips', [])):
#             return Response(status=403, response=str(request.remote_addr))
#         return func(*args, **kwargs)
#
#     return decorated_view
