import base64
import os
import random
import smtplib
import sqlite3
import subprocess
import time
import ujson
import yaml

from flask import session

from dulwich.repo import Repo

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from twilio.rest import Client as TwilioRestClient

import logger

from decorators import async


IMAGE_EXTS = ('jpg', 'png', 'jpeg', 'svg', 'apng', 'gif', 'bmp',)
RANDOM_CHARS = list('abcdefghijklmnopqrstuvwxyz0123456789')

@async
def send_mail(send_to, subject, body):
    if isinstance(send_to, str):
        send_to = [send_to]

    conf = get_config().get('mail', {})

    host = conf.get('host')
    user = conf.get('user')
    password = conf.get('password')

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = user
    msg['To'] = ', '.join(send_to)
    msg.attach(MIMEText(body, 'html'))

    server = smtplib.SMTP(host)
    server.starttls()
    server.login(user, password)
    server.sendmail(user, send_to, msg.as_string())
    server.quit()


def get_config():
    # rewriting this on failure to find the file might be dangerous ?
    if not os.path.isfile('./config.yaml'):
        with open('./config.yaml', 'w+') as f:
            yaml.dump({'app': {'SECRET_KEY': base64.b64encode(os.urandom(30)),
                               'debug': True,
                               'port': 5000,
                               'upload_folder': './uploads'},
                       'twilio': {'sid': 'SID',
                                  'auth': 'AUTH',
                                  'from': ['+15555555555']},
                       'mail': {'host': 'smtp.gmail.com:587',
                                'user': 'user@gmail.com',
                                'password': 'password',
                                'contact': 'user@gmail.com'}}, f, default_flow_style=False)

    with open('./config.yaml') as f:
        return yaml.load(f)


def send_sms(to, body):
    conf = get_config().get('twilio', {})
    client = TwilioRestClient(conf.get('sid'), conf.get('auth'))

    message = client.messages.create(to=to,
                                     from_=random.choice(conf.get('from')),
                                     body=body)


def update_access(userid, access):
    with sqlite3.connect('./data/database.db') as c:
        c.execute('UPDATE users SET access = ? WHERE id = ?',
                  (access, userid,))

    reload_session(userid)


def force_logout(userid):
    with sqlite3.connect('./data/database.db') as c:
        c.execute('INSERT INTO sessions VALUES (?, ?, ?)', (userid, int(time.time()), 'force_logout',))


def reload_session(userid):
    with sqlite3.connect('./data/database.db') as c:
        c.execute('INSERT INTO sessions VALUES (?, ?, ?)', (userid, int(time.time()), 'reload_session',))


def clear_session_actions(userid, retrieve=False):
    with sqlite3.connect('./data/database.db') as c:
        if retrieve:
            actions = c.execute('SELECT flag FROM sessions WHERE user = ?', (session.get('id'),)).fetchall()
        c.execute('DELETE FROM sessions WHERE user = ?', (userid,))

    if not retrieve:
        return

    return list(set([x[0] for x in actions]))


def login(row):
    # row = SELECT id, email, password, name, phone, access, prefs FROM users

    # we don't want to do any session actions in a fresh session (might change in the future ?)
    clear_session_actions(row[0])

    session['id'] = row[0]
    session['email'] = row[1]
    session['name'] = row[3]
    session['avatar'] = row[4]
    session['phone'] = row[5]
    session['access'] = [x.strip() for x in row[6].split(',')]
    session['prefs'] = ujson.loads(row[7])

    if any(['unverified' in item for item in session['access']]):
        session['profile_alert'] = True

    logger.log('LOGIN: %s' % (ujson.dumps(session['access']),), userid=row[0])


def get_ext(filename):
    ext = filename.split('.')[-1].lower()
    if filename.lower() == ext or not ext.strip():
        return 'noext'
    return ext


def random_name(length=6):
    return ''.join([random.choice(RANDOM_CHARS) for x in range(0, length)])


def is_image(path):
    return path.split('.')[-1].lower() in IMAGE_EXTS
