import sqlite3
import time


# D I W E C
def log(message, userid=0, level='I'):
    with sqlite3.connect('./data/database.db') as c:
        c.execute('INSERT INTO logs VALUES (NULL, ?, ?, ?, ?)', (time.time(), level, userid, message,))


def get_last(num=1000, level='any', userid=-1, offset=0):
    where = []
    items = []
    offset = [offset] if offset else []

    if level != 'any':
        where.append('level = ?')
        items.append(level)

    if userid != -1:
        where.append('user = ?')
        items.append(userid)

    with sqlite3.connect('./data/database.db') as c:
        rows = c.execute('SELECT timestamp, level, user, message FROM logs '+' '.join(where)+' ORDER BY timestamp DESC LIMIT ?'+(' OFFSET ?' if offset else ''),
                         where + [num] + offset)

    return reversed(rows)
