import os
import importlib

from flask import Flask, redirect, render_template, request, session
from flask_cors import CORS
from waitress import serve

import logger
import utils

app = Flask(__name__, static_folder = '../web/build')
config = utils.get_config()
app.config.update(config.get('app', {}))
if os.environ.get('PRODUCTION') == 'false':
    CORS(app, resources={r"/api/*": {"origins": "*"}}, supports_credentials=True)


# load all blueprints from blueprints dir
blueprints = [f.split('.')[0] for f in os.listdir('blueprints') if f.endswith('.py') and not f.startswith('_')]
for b in blueprints:
    module = importlib.import_module('.' + b, 'blueprints')
    try:
        app.register_blueprint(module.blueprint)

    except Exception as e:
        logger.log('BLUEPRINT FAILURE: %s:%s' % (b, e,), level='C')
        exit(1)


if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=5000)
