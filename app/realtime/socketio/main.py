import eventlet
eventlet.monkey_patch()
import os

from flask import Flask
from flask_socketio import SocketIO, send, emit
from threading import Thread
import rethinkdb as r
from rethinkdb import RqlRuntimeError

app = Flask(__name__)
socketio = SocketIO(app, logger=True, engineio_logger=True)
global thread
thread = None


def watch_realtime():
    print 'Watching db for new messages!'
    conn = r.connect(host='rethinkdb', 
                     port=28015, 
                     db='chat')
    feed = r.table("chats").changes().run(conn)
    for chat in feed:
        chat['new_val']['created'] = str(chat['new_val']['created'])
        socketio.emit('new_chat', chat)


if __name__ == "__main__":
    if thread is None:
        thread = Thread(target=watch_realtime)
        thread.start()
    socketio.run(app, host='0.0.0.0', port=8000)
