let apiHost = 'http://localhost'

if (process.env.NODE_ENV === 'production') {
  apiHost = 'https://boilerplate.com'
}

export default apiHost
