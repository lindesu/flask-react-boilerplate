import App from './containers/App'
import Splash from './containers/Splash'
import Login from './containers/Login'
import Register from './containers/Register'
import Profile from './containers/Profile'
import UserManager from './containers/UserManager'
import Logs from './containers/Logs'
import Settings from './containers/Settings'


const routes = [
  { component: App,
    routes: [
      { path: '/',
        exact: true,
        component: Splash
      },
      { path: '/login',
        exact: true,
        component: Login
      },
      { path: '/register',
        exact: true,
        component: Register
      },
      { path: '/profile/:userName',
        exact: true,
        component: Profile
      },
      { path: '/settings',
        exact: true,
        component: Settings
      },
      { path: '/admin/users',
        exact: true,
        component: UserManager
      },
      { path: '/admin/logs',
        exact: true,
        component: Logs
      },
    ]
  }
]


export default routes
