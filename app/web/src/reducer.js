import Immutable from 'immutable'

const initialState = Immutable.fromJS({
  user: {},
  profile: {},
  logs: {},
  users: {},
  messages: {},
})

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER':
      return state.set('user', Immutable.fromJS(action.data))
    case 'SET_PROFILE':
      return state.set('profile', Immutable.fromJS(action.data))
    case 'SET_LOGS':
      return state.set('logs', Immutable.fromJS(action.data))
    case 'SET_USERS':
      return state.set('users', Immutable.fromJS(action.data))
    case 'SET_MESSAGES':
      return state.set('messages', Immutable.fromJS(action.data))  
    default:
      return state
  }
}
