import React from 'react'
import { connect } from 'react-redux'
import { getUser } from '../actions'
import { withRouter } from 'react-router'
import { renderRoutes } from 'react-router-config'
import reactRouterFetch from 'react-router-fetch'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import NavBar from '../components/NavBar'
import './styles.css'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
})

class App extends React.Component {
  state = {
    isFetching: false,
    fetchingError: null
  }

  componentWillMount () {
    this.fetchRoutes(this.props)
  }

  componentWillReceiveProps (nextProps) {
    const current = `${this.props.location.pathname}${this.props.location.search}`
    const next = `${nextProps.location.pathname}${nextProps.location.search}`
    if (current === next) {
     return
    }
    this.fetchRoutes(nextProps)
  }

  shouldComponentUpdate (nextProps, nextState) {
    return !nextState.isFetching
  }

  fetchRoutes (props) {
    const { route, dispatch, location } = props
    this.setState({
      isFetching: true,
      fetchingError: null
    })
    this.props.dispatch(getUser()).then(() => {
      reactRouterFetch(route.routes, location, { dispatch })
      .then((results) => {
        this.setState({
          isFetching: false
        })
      })
      .catch((err) => {
        this.setState({
          isFetching: false,
          fetchingError: err
        })
      })
    })
  }

  renderLayout() {
    const { route, location } = this.props

    return (
      <main className={'main'}>
        <NavBar location={location} />
        <div className={'content'}>
          {renderRoutes(route.routes)}
        </div>
      </main>
    )
  }

  render() {
    const { isFetching } = this.state

    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        {!isFetching && this.renderLayout()}
      </MuiThemeProvider>
    )
  }
}

export default withRouter(connect()(App))
