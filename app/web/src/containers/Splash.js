import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Chat from '../components/Chat'
import { connect } from 'react-redux'
import { getMessages } from '../actions'


const styles = theme => ({
  splash: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginTop: 54,
    justifyContent: 'space-around',
    [theme.breakpoints.down('sm')]: {
      marginTop: 44,
    },
  },
  brand: {
    [theme.breakpoints.down('sm')]: {
     fontSize: '4em',
    },
  },
  subtitle: {
    [theme.breakpoints.down('sm')]: {
     fontSize: '1.3em',
    },
  },
  chat: {
    marginTop: 24,
    width: '50%',
    minWidth: 400,
  },
})

class Splash extends React.PureComponent {
  static fetch(match, location, options) {
    return options.dispatch(getMessages())
  }

  render() {
    const { classes } = this.props

    return (
      <div className={classes.splash}>
        <Typography variant='h2' className={classes.brand}>
          Realtime Boilerplate
        </Typography>
        <Typography variant='h5' className={classes.subtitle}>
          A React frontend for a Flask backend. Socketio and RethinkDB for realtime JSON.
        </Typography>
        <Typography variant='h5' className={classes.subtitle}>
          v0.01
        </Typography>
        <div className={classes.chat}><Chat /></div>
      </div>
    )
  }
}

export default withStyles(styles)(connect()(Splash))

