import React from 'react'
import Immutable from 'immutable'
import { updateUserInfo, updateUserAvatar, updateUserPassword } from '../actions'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import SwipeableViews from 'react-swipeable-views'
import AppBar from '@material-ui/core/AppBar'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'

const styles = theme => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    textAlign: 'center',
    justifyContent: 'space-around',
    marginTop: 28,
    [theme.breakpoints.down('sm')]: {
      marginTop: 20,
    },
  },
  appbar: {
    boxShadow: 'none',
    border: 0,
    backgroundColor: '#fafafa',
    zIndex: 0,
  },
  extraMargin: {
    marginBottom: 20,
  },
  listText: {
    padding: 0
  },
  formControl: {
    margin: theme.spacing.unit * 2,
    display: 'flex',
    width: 280,
  },
  inputLabelFocused: {
   color: '#E91E63',
  },
  inputInkbar: {
    '&:after': {
      backgroundColor: '#E91E63',
    },
  },
  settings: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    textAlign: 'center',
  }
})

const mapStateToProps = state => ({
  user: state.get('user'),
})

function TabContainer({ children, dir }) {
  return (
    <div dir={dir} style={{ padding: 8 }}>
      {children}
    </div>
  )
}

class Settings extends React.Component {
  state = {
    dialog: false,
    file: null,
    value: 0,
    emailInput: this.props.user.get('email'),
    nameInput: this.props.user.get('name'),
    oldPWInput: '',
    newPWInput: '',
    newPWConfirmInput: ''
  }

  componentWillMount() {
    const { user, history } = this.props
    !user.get('email') && history.push('/login')
  }

  componentDidUpdate() {
    const { user, history } = this.props
    !user.get('email') && history.push('/login')
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (!Immutable.is(this.props.user, nextProps.user) || this.state !== nextState)
  }

  onDetailsSubmit = () => {
    const { emailInput, nameInput } = this.state
    this.props.dispatch(updateUserInfo(emailInput, nameInput))
  }

  onFileSubmit = () => {
    const formData = new FormData()
    formData.append('file', this.state.file)
    this.props.dispatch(updateUserAvatar(formData))
  }

  onPasswordSubmit = () => {
    const { oldPWInput, newPWInput, newPWConfirmInput } = this.state
    if (newPWInput === newPWConfirmInput) {
      this.props.dispatch(updateUserPassword(oldPWInput, newPWConfirmInput))
    }
  }

  handleFileChange = (event) => {
    this.setState({ file: event.target.files[0] })
  }

  handleFormChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  handleChangeIndex = index => {
    this.setState({ value: index })
  }

  render() {
    const { emailInput, nameInput, oldPWInput, newPWInput, newPWConfirmInput } = this.state
    const { classes, theme } = this.props

    return (
      <div className={classes.container}>
        <AppBar position='static' color="default" className={classes.appbar}>
         <Tabs
           value={this.state.value}
           onChange={this.handleChange}
           indicatorColor="secondary"
           textColor="secondary"
           fullWidth
           centered
         >
           <Tab label="Details" />
           <Tab label="Avatar" />
           <Tab label="Password" />
         </Tabs>
        </AppBar>
        <SwipeableViews
         axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
         index={this.state.value}
         onChangeIndex={this.handleChangeIndex}
        >
          <TabContainer dir={theme.direction}>
            <div className={classes.settings} autoComplete="off" onSubmit={this.onFormSubmit}>
              <FormControl className={classes.formControl}>
                <InputLabel
                  required
                  shrink
                  className={classes.textField}
                  htmlFor="email">
                  Email
                </InputLabel>
                <Input
                  id="email"
                  className={classes.textField}
                  value={emailInput}
                  onChange={this.handleFormChange('emailInput')}
                  classes={{
                    
                  }}
                  />
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel
                 required
                 shrink
                 className={classes.textField}
                 htmlFor="name">
                 Name
                </InputLabel>
                <Input
                 id="name"
                 className={classes.textField}
                 value={nameInput}
                 onChange={this.handleFormChange('nameInput')}
                 classes={{
                  
                 }}
                 />
              </FormControl>
              <Button color="secondary" onClick={this.onDetailsSubmit}>
                Save Details
              </Button>
            </div>
          </TabContainer>
          <TabContainer dir={theme.direction}>
             <div className={classes.settings} autoComplete="off">
               <FormControl className={classes.formControl}>
                 <input id='avatar' type="file" onChange={this.handleFileChange} />
               </FormControl>
               <Button color="secondary" onClick={this.onFileSubmit}>
                 Save Avatar
               </Button>
             </div>
          </TabContainer>
          <TabContainer dir={theme.direction}>
              <div className={classes.settings} autoComplete="off">
                <FormControl className={classes.formControl}>
                  <InputLabel
                    required
                    shrink
                    className={classes.textField}
                    htmlFor="oldPW">
                    Old Password
                  </InputLabel>
                  <Input
                    id="oldPW"
                    type='password'
                    className={classes.textField}
                    value={oldPWInput}
                    onChange={this.handleFormChange('oldPWInput')}
                    classes={{
                    }}
                    />
                 </FormControl>
                 <FormControl className={classes.formControl}>
                   <InputLabel
                     required
                     shrink
                     className={classes.textField}
                     htmlFor="newPW">
                     New Password
                   </InputLabel>
                   <Input
                     id="newPW"
                     type='password'
                     className={classes.textField}
                     value={newPWInput}
                     onChange={this.handleFormChange('newPWInput')}
                     classes={{
                     }}
                     />
                 </FormControl>
                 <FormControl className={classes.formControl}>
                   <InputLabel
                     required
                     shrink
                     className={classes.textField}
                     htmlFor="newPW">
                     Confirm Password
                   </InputLabel>
                   <Input
                     id="newPWConfirm"
                     type='password'
                     className={classes.textField}
                     value={newPWConfirmInput}
                     onChange={this.handleFormChange('newPWConfirmInput')}
                     classes={{
                     }}
                     />
                 </FormControl>
                 <Button color="secondary" onClick={this.onPasswordSubmit}>
                   Save Password
                 </Button>
              </div>
          </TabContainer>
         </SwipeableViews>
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(Settings))
