import React from 'react'
import { connect } from 'react-redux'
import { getLogs } from '../actions'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'

const styles = theme => ({
  container: {
    height: '100%',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    marginTop: 28,
    [theme.breakpoints.down('sm')]: {
      marginTop: 20,
    },
  },
  table: {
    width: 300,
    boxShadow: 'none',
    border: 0,
    backgroundColor: '#fafafa',
  },
  tableCell: {
    maxWidth: 75,
    border: 0,
    [theme.breakpoints.up('sm')]: {
      maxWidth: 125,
    },
  },
  tableRow: {
    height: 30,
  },
  button: {
    marginTop: theme.spacing.unit * 3,
  },
  raisedAccent: {
   color: theme.palette.getContrastText('#C8C8C8'),
   backgroundColor: '#C8C8C8',
   '&:hover': {
     backgroundColor: '#E91E63',
     // Reset on mouse devices
     '@media (hover: none)': {
       backgroundColor: '#E91E63',
     },
    },
   },
  link: {
    border: 'none',
    outline: 'none',
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
  title: {
    marginTop: 8,
    [theme.breakpoints.down('sm')]: {
      fontSize: '2em',
    },
  },
})

const mapStateToProps = state => ({
  user: state.get('user'),
  logs: state.get('logs'),
})

class Logs extends React.Component {
  static fetch(match, location, options) {
    return options.dispatch(getLogs())
  }

  componentWillMount() {
    const { user, history } = this.props
    const admin = user.get('access') ? user.get('access').toJS().includes('admin') : false
    !admin && history.push(`/`)
  }

  componentDidUpdate() {
    const { user, history } = this.props
    const admin = user.get('access') ? user.get('access').toJS().includes('admin') : false
    !admin && history.push(`/`)
  }

  renderLogs() {
    const { classes, logs } = this.props

    return (
      <List className={classes.root} subheader={<li />}>
        {logs.get('logs').toJS().map((log, index) => (
          <ListItem key={`item-${index}`}>
            <ListItemText primary={`${index} ${log[0]} ${log[1]} ${log[2]} ${log[3]} ${log[4]} ${log[5]}`} />
          </ListItem>
        ))}
      </List>
    )
  }

  render() {
    const { classes, logs } = this.props

    return (
      <div className={classes.container}>
        <Typography variant="h2" className={classes.title}>System Logs</Typography>
        {logs.get('logs') ? this.renderLogs() : <div />}
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(Logs))
