import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { adminCreateUser, adminUpdateUserInfo, adminUpdateUserAccess, adminResetPassword, adminDisableUser, getUsers } from '../actions'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import SettingsIcon from '@material-ui/icons/Settings'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle' 
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const styles = theme => ({
  container: {
    height: '100%',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    marginTop: 28,
    [theme.breakpoints.down('sm')]: {
      marginTop: 20,
    },
  },
  userRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  button: {
    marginTop: theme.spacing.unit,
  },
  raisedAccent: {
   color: theme.palette.getContrastText('#C8C8C8'),
   backgroundColor: '#C8C8C8',
   '&:hover': {
     backgroundColor: '#E91E63',
     // Reset on mouse devices
     '@media (hover: none)': {
       backgroundColor: '#E91E63',
     },
    },
   },
  link: {
    border: 'none',
    outline: 'none',
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
  title: {
    marginTop: 8,
    [theme.breakpoints.down('sm')]: {
      fontSize: '2em',
    },
  },
})

const mapStateToProps = state => ({
  user: state.get('user'),
  users: state.get('users'),
})

class UserManager extends React.Component {
  static fetch(match, location, options) {
    return options.dispatch(getUsers())
  }

  state = {
    expanded: null,
    anchorEl: null,
    createDialog: false,
    editInfoDialog: false,
    editAccessDialog: false,
    resetPasswordDialog: false,
    userDisableDialog: false,
    loadedUsername: '',
    loadedEmail: '',
    loadedID: '',
    loadedUserAccess: '',
    username: '',
    email: '',
    testData: '',
  }

  componentWillMount() {
    const { user, history } = this.props
    const admin = user.get('access') ? user.get('access').toJS().includes('admin') : false
    !admin && history.push(`/`)
  }

  componentDidUpdate() {
    const { user, history } = this.props
    const admin = user.get('access') ? user.get('access').toJS().includes('admin') : false
    !admin && history.push(`/`)
  }

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    })
  }

  handleClick = user => event => {
    this.setState({
      loadedID: user[0],
      loadedUsername: user[3],
      loadedEmail: user[1],
      loadedUserAccess: user[5],
      anchorEl: event.currentTarget
     })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleCreateOpen = () => {
    this.setState({ createDialog: true })
  }

  handleCreateClose = () => {
    this.setState({ createDialog: false })
  }

  handleFormChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  postCreateData() {
    this.props.dispatch(adminCreateUser(this.state.username, this.state.email, this.state.testData.length))
    this.setState({
      username: '',
      email: '',
      testData: '',
    })
  }

  postEditData() {
    this.props.dispatch(adminUpdateUserInfo(this.state.loadedID, this.state.loadedUsername, this.state.loadedEmail))
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      editInfoDialog: false
    })
  }

  postAccessData() {
    this.props.dispatch(adminUpdateUserAccess(this.state.loadedID, this.state.loadedUserAccess))
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      editAccessDialog: false
    })
  }

  postDisable() {
    this.props.dispatch(adminDisableUser(this.state.loadedID))
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      userDisableDialog: false
    })
  }

  postResetPassword() {
    this.props.dispatch(adminResetPassword(this.state.loadedID))
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      resetPasswordDialog: false
    })
  }

  handleCreateUser = () => {
    const { username, email } = this.state

    if ( username && email) {
      this.postCreateData()
    }
  }

  handleOpenEdit = () => {
    this.setState({
      editInfoDialog: true,
      anchorEl: null
    })
  }

  handleOpenAccess = () => {
    this.setState({
      editAccessDialog: true,
      anchorEl: null
    })
  }

  handleOpenDisable = () => {
    this.setState({
      userDisableDialog: true,
      anchorEl: null
    })
  }

  handleOpenReset = () => {
    this.setState({
      resetPasswordDialog: true,
      anchorEl: null
    })
  }

  handleEditClose = () => {
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      editInfoDialog: false
     })
  }

  handleAccessClose = () => {
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      editAccessDialog: false
     })
  }

  handleDisableClose = () => {
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      userDisableDialog: false
     })
  }

  handleResetClose = () => {
    this.setState({
      loadedID: '',
      loadedUsername: '',
      loadedEmail: '',
      loadedUserAccess: '',
      resetPasswordDialog: false
     })
  }

  handleEditUser = () => {
    const { loadedUsername, loadedEmail } = this.state

    if ( loadedUsername && loadedEmail ) {
      this.postEditData()
    }
  }

  handleAccessUser = () => {
    const { loadedID, loadedUserAccess } = this.state

    if ( loadedID && loadedUserAccess ) {
      this.postAccessData()
    }
  }

  handleDisableUser = () => {
    const { loadedID } = this.state

    if ( loadedID ) {
      this.postDisable()
    }
  }

  handleResetPassword = () => {
    const { loadedID } = this.state

    if ( loadedID ) {
      this.postResetPassword()
    }
  }

  renderUsers() {
    const { classes, users } = this.props
    const { anchorEl } = this.state

    return (
      <List className={classes.root}>
        {users.get('users').toJS().map((user, index) => (
          <ListItem key={`user-item-${index}`}>
            <Link to={`/profile/${user[3]}`} className={classes.link}>
              <ListItemText primary={`${user[3]} | ${user[1]} | ${user[5]}`} />
            </Link>
            <ListItemSecondaryAction>
              <IconButton
                aria-label='Manage'
                aria-owns={anchorEl ? `simple-menu-${index}` : null}
                aria-haspopup="true"
                onClick={this.handleClick(user)}>
                <SettingsIcon />
              </IconButton>
              <Menu
               id={`simple-menu-${index}`}
               anchorEl={anchorEl}
               open={Boolean(anchorEl)}
               onClose={this.handleClose}
             >
               <MenuItem onClick={this.handleOpenEdit}>Edit Info</MenuItem>
               <MenuItem onClick={this.handleOpenAccess}>Change Access</MenuItem>
               <MenuItem onClick={this.handleOpenReset}>Reset Password</MenuItem>
               <MenuItem onClick={this.handleOpenDisable}>Disable Account</MenuItem>
             </Menu>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    )
  }

  renderEditInfo() {
    return (
        <Dialog
          open={this.state.editInfoDialog}
          onClose={this.handleEditClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Edit User Information</DialogTitle>
          <DialogContent>
            <TextField
              margin="dense"
              id="name"
              label="Username"
              type="name"
              value={this.state.loadedUsername}
              onChange={this.handleFormChange('loadedUsername')}
              fullWidth
            />
            <TextField
              margin="dense"
              id="email"
              label="Email Address"
              type="email"
              value={this.state.loadedEmail}
              onChange={this.handleFormChange('loadedEmail')}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleEditClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleEditUser} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
    )
  }

  renderEditAccess() {
    return (
        <Dialog
          open={this.state.editAccessDialog}
          onClose={this.handleAccessClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Edit User Access</DialogTitle>
          <DialogContent>
            <TextField
              margin="dense"
              id="access"
              label="Access"
              type="text"
              value={this.state.loadedUserAccess}
              onChange={this.handleFormChange('loadedUserAccess')}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAccessClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleAccessUser} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
    )
  }


  renderDisableUser() {
    return (
        <Dialog
          open={this.state.userDisableDialog}
          onClose={this.handleDisableClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Disable User</DialogTitle>
          <DialogActions>
            <Button onClick={this.handleDisableClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleDisableUser} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
    )
  }


  renderResetPassword() {
    return (
        <Dialog
          open={this.state.resetPasswordDialog}
          onClose={this.handleResetClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Reset User Password</DialogTitle>
          <DialogActions>
            <Button onClick={this.handleResetClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleResetPassword} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
    )
  }

  render() {
    const { classes, users } = this.props

    return (
      <div className={classes.container}>
        <Typography variant="h2" className={classes.title}>Manage Users</Typography>
        <Button onClick={this.handleCreateOpen} color="secondary" className={classes.button}>
          Create User
        </Button>
        <Dialog
          open={this.state.createDialog}
          onClose={this.handleCreateClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Create New User</DialogTitle>
          <DialogContent>
            <TextField
              margin="dense"
              id="name"
              label="Username"
              type="name"
              value={this.state.username}
              onChange={this.handleFormChange('username')}
              fullWidth
            />
            <TextField
              margin="dense"
              id="email"
              label="Email Address"
              type="email"
              value={this.state.email}
              onChange={this.handleFormChange('email')}
              fullWidth
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={this.state.testData}
                  onChange={this.handleFormChange('testData')}
                />
              }
              label="Load with test data"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCreateClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleCreateUser} color="primary">
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
        {this.renderEditInfo()}
        {this.renderEditAccess()}
        {this.renderDisableUser()}
        {this.renderResetPassword()}
        {users.get('users') ? this.renderUsers() : <div />}
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(UserManager))
