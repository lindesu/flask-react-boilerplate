import React from 'react'
import apiHost from '../apiHost'
import { connect } from 'react-redux'
import { getProfile } from '../actions'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginTop: 54,
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      marginTop: 44,
    },
  },
  appbar: {
    boxShadow: 'none',
    border: 0,
    backgroundColor: '#fafafa',
    zIndex: 0,
  },
  details: {
    fontSize: 20,
    [theme.breakpoints.up('sm')]: {
      fontSize: 22,
    },
  },
  card: {
   width: 300,
   maxWidth: 345,
   textAlign: 'center',
   boxShadow: 'none',
   border: 0,
   backgroundColor: '#fafafa',
   [theme.breakpoints.up('sm')]: {
     maxWidth: 360,
     width: 360,
   },
  },
  media: {
   height: 300,
   width: 300,
   maxWidth: 345,
   [theme.breakpoints.up('sm')]: {
     height: 360,
     width: 360,
     maxWidth: 360,
   },
  },
  notFound: {
    marginTop: 16,
    fontSize: 20,
    [theme.breakpoints.up('sm')]: {
      fontSize: 25,
    },
  },
})

const mapStateToProps = state => ({
  profile: state.get('profile'),
})

class Profile extends React.PureComponent {
  static fetch(match, location, options) {
    return options.dispatch(getProfile(match.params.userName))
  }

  renderProfile() {
    const { classes, profile } = this.props

    return (
      <div className={classes.container}>
        <Card className={classes.card}>
          <CardMedia
            className={classes.media}
            image={`${apiHost}${profile.get('avatar')}`}
            title="Avatar"
          />
          <CardContent>
            <Typography className={classes.details} type="h4">
             {profile.get('email')}
            </Typography>
          </CardContent>
        </Card>
      </div>
    )
  }

  render() {
    const { classes, profile } = this.props

    if ( !profile.get('email') ) {
      return <div className={classes.container}><Typography className={classes.notFound} type="h4">User not found.</Typography></div>
    } else {
      return this.renderProfile()
    }
  }
}

export default withStyles(styles)(connect(mapStateToProps)(Profile))
