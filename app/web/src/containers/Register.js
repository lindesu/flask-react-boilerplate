import React from 'react'
import { registerUser } from '../actions'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'

const styles = theme => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginTop: 90,
    justifyContent: 'space-around',
    [theme.breakpoints.down('sm')]: {
      marginTop: 80,
    },
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 280,
  },
  menu: {
    width: 280,
  },
  button: {
    margin: theme.spacing.unit * 3,
    width: 200,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  raisedAccent: {
   color: theme.palette.getContrastText('#C8C8C8'),
   backgroundColor: '#C8C8C8',
   '&:hover': {
     backgroundColor: '#E91E63',
     // Reset on mouse devices
     '@media (hover: none)': {
       backgroundColor: '#E91E63',
     },
    },
   },
   inputLabelFocused: {
    color: '#E91E63',
   },
   inputInkbar: {
     '&:after': {
       backgroundColor: '#E91E63',
     },
   },
})

class Register extends React.Component {
  state = {
    email: '',
    name: '',
  }

  postData() {
    const { history } = this.props
    const { email, name } = this.state
    this.props.dispatch(registerUser(email, name))
    this.setState({
      email: '',
      name: '',
    })
    history.push('/login')
  }

  handleSubmit = () => {
    const { email, name } = this.state

    if ( email && name ) {
      this.postData()
    }
  }

  handleKeyPress = (event) => {
    const { email, name } = this.state

    if ( event.key === 'Enter' ) {
      if ( email && name ) {
        this.postData()
      }
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  render() {
    const { classes } = this.props
    const { email, name } = this.state

    return (
      <form className={classes.container} noValidate autoComplete="on">
        <FormControl className={classes.formControl}>
          <InputLabel
            required
            shrink
            className={classes.textField}
            htmlFor="email">
            Email
          </InputLabel>
          <Input
            id="email"
            autoFocus
            className={classes.textField}
            value={email}
            onChange={this.handleChange('email')}
            onKeyDown={this.handleKeyPress}
            classes={{
            }}
            />
         </FormControl>
         <FormControl className={classes.formControl}>
           <InputLabel
             required
             shrink
             className={classes.textField}
             htmlFor="name">
             Username
           </InputLabel>
           <Input
             id="name"
             className={classes.textField}
             value={name}
             onChange={this.handleChange('name')}
             onKeyDown={this.handleKeyPress}
             classes={{
             }}
             />
          </FormControl>
          <Button onClick={this.handleSubmit} className={classes.button} classes={{ raisedSecondary: classes.raisedAccent }} color="secondary">
            Register
          </Button>
      </form>
    )
  }
}

export default withStyles(styles)(connect()(Register))
