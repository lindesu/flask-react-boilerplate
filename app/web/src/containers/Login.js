import React from 'react'
import Immutable from 'immutable'
import { connect } from 'react-redux'
import { loginUser } from '../actions'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'

const styles = theme => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginTop: 90,
    justifyContent: 'space-around',
    [theme.breakpoints.down('sm')]: {
      marginTop: 80,
    },
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 280,
  },
  menu: {
    width: 280,
  },
  button: {
    margin: theme.spacing.unit * 3,
    width: 200,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  formControl: {
   margin: theme.spacing.unit,
  },
  raisedAccent: {
   color: theme.palette.getContrastText('#C8C8C8'),
   backgroundColor: '#C8C8C8',
   '&:hover': {
     backgroundColor: '#E91E63',
     // Reset on mouse devices
     '@media (hover: none)': {
       backgroundColor: '#E91E63',
     },
    },
   },
   inputLabelFocused: {
    color: '#E91E63',
   },
   inputInkbar: {
     '&:after': {
       backgroundColor: '#E91E63',
     },
   },
})

const mapStateToProps = state => ({
  user: state.get('user'),
})

class Login extends React.Component {
  state = {
    email: '',
    password: '',
  }

  componentWillMount() {
    const { user, history } = this.props
    user.get('email') && history.push(`/profile/${user.get('name')}`)
  }

  componentDidUpdate() {
    const { user, history } = this.props
    user.get('email') && history.push(`/profile/${user.get('name')}`)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (!Immutable.is(this.props.user, nextProps.user) || this.state !== nextState)
  }

  postData() {
    this.props.dispatch(loginUser(this.state.email, this.state.password))
    this.setState({
      email: '',
      password: '',
    })
  }

  handleSubmit = () => {
    const { email, password } = this.state

    if ( email && password ) {
      this.postData()
    }
  }

  handleKeyPress = (event) => {
    const { email, password } = this.state

    if ( event.key === 'Enter' ) {
      if ( email && password ) {
        this.postData()
      }
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  renderForm() {
    const { classes } = this.props
    const { email, password } = this.state

    return (
      <form className={classes.container} noValidate autoComplete="on">
        <FormControl className={classes.formControl}>
          <InputLabel
            required
            shrink
            className={classes.textField}
            htmlFor="email">
            Email
          </InputLabel>
          <Input
            autoFocus
            id="email"
            className={classes.textField}
            value={email}
            onChange={this.handleChange('email')}
            onKeyDown={this.handleKeyPress}
            classes={{
            }}
           />
        </FormControl>
        <FormControl className={classes.formControl}>
         <InputLabel
           required
           shrink
           className={classes.textField}
           htmlFor="password">
           Password
         </InputLabel>
         <Input
           id="password"
           type="password"
           className={classes.textField}
           value={password}
           onChange={this.handleChange('password')}
           onKeyDown={this.handleKeyPress}
           classes={{
           }}
          />
        </FormControl>
        <Button onClick={this.handleSubmit} className={classes.button} classes={{ raisedSecondary: classes.raisedAccent }} color="secondary">
          Login
        </Button>
      </form>
    )
  }

  render() {
    const { user } = this.props

    if ( user.get('email') ) {
      return <div />
    } else {
      return this.renderForm()
    }
  }
}

export default withStyles(styles)(connect(mapStateToProps)(Login))
