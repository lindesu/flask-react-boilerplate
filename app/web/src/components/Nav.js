import React from 'react'
import Immutable from 'immutable'
import { connect } from 'react-redux'
// import { NavLink } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import compose from 'recompose/compose'
import withWidth from '@material-ui/core/withWidth'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
// import ListItemText from '@material-ui/core/ListItemText'
// import ListItem from '@material-ui/core/ListItem'
import MenuIcon from '@material-ui/icons/Menu'
import IconButton from '@material-ui/core/IconButton'
// import Hidden from '@material-ui/core/Hidden'

const styles = theme => ({
  flex: {
    flex: 1,
  },
  menuButton: {
    marginRight: 20,
  },
  link: {
    border: 'none',
    outline: 'none',
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
  drawer: {
    marginTop: 64
  },
  drawerPaper: {
    width: 250,
    backgroundColor: '#f5f5f5',
    boxShadow: 'none',
    border: 0,
  },
  drawerDesktopPaper: {
    width: 250,
    backgroundColor: '#f5f5f5',
    boxShadow: 'none',
    border: 0,
    marginTop: 64
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
})

const mapStateToProps = state => ({
  user: state.get('user'),
})

class Nav extends React.Component {
  state = {
    nav: false,
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.width !== 'xs' || nextProps.width !== 'sm') {
      this.setState({
        nav: false
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (!Immutable.is(this.props.user, nextProps.user) || this.state !== nextState || this.props.location !== nextProps.location)
  }

  toggleDrawer = (open) => () => {
    this.setState({
      nav: open
    })
  }

  render() {
    const { nav } = this.state
    const { classes } = this.props

    return (
      <div>
        <IconButton onClick={this.toggleDrawer(!nav)} className={classes.menuButton} color="default" aria-label="Menu">
          <MenuIcon />
        </IconButton>
        <Drawer variant="temporary" open={nav} onClose={this.toggleDrawer(!nav)} classes={{ paper: classes.drawerPaper }} ModalProps={{keepMounted: true,}}>
          <div>
            <List disablePadding>
            {/* <NavLink to='/mlb' className={classes.link} activeStyle={{ backgroundColor: '#E0E0E0' }}>
                <ListItem button className={classes.nested}>
                  <ListItemText primary="MLB" />
                </ListItem>
              </NavLink> */}
            </List>
          </div>
        </Drawer>
        </div>
    )
  }
}

export default compose(withStyles(styles), withWidth())(connect(mapStateToProps)(Nav))
