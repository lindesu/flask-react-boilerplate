import React from 'react';
import { NavLink } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
// import Nav from './Nav';
import UserMenu from './UserMenu';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  flex: {
    flex: 1,
  },
  link: {
    border: 'none',
    outline: 'none',
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
  bar: {
    boxShadow: 'none',
    border: 0,
    backgroundColor: '#C8C8C8',
    marginBottom: 64,
  },
});

class NavBar extends React.PureComponent {

  render() {
    // const { classes, location } = this.props;
    const { classes } = this.props;
    return (
      <AppBar position='fixed' className={classes.bar}>
        <Toolbar>
          {/* <Nav location={location} /> */}
          <Typography variant='h6' color='inherit'>
            <NavLink to='/' className={classes.link}>Boilerplate</NavLink>
          </Typography>
          <div className={classes.flex} />
          <UserMenu />
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(NavBar);
