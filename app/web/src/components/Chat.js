import React from 'react'
import apiHost from '../apiHost'
import { withStyles } from '@material-ui/core/styles'
import Immutable from 'immutable'
import { connect } from 'react-redux'
import { createMessage } from '../actions'
import io from 'socket.io-client'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField';


const styles = theme => ({
  chat: {
    width: 600,
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: 15,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
})

const mapStateToProps = state => ({
  user: state.get('user'),
  messages: state.get('messages'),
})

class Chat extends React.Component {
  state = {
    create_message: '',
    chat: this.props.messages.toJS() || [],
  }

  componentWillMount() {
    const host = `${apiHost}:8000`
    const socket = io(host, {reconnect: true})
    socket.on('new_chat', (data) => {
      this.setState({
        chat: [data.new_val, ...this.state.chat],
      })
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (!Immutable.is(this.props.user, nextProps.user) || this.state !== nextState)
  }

  postData() {
    const { user } = this.props
    const { create_message } = this.state
    this.props.dispatch(createMessage(user.get('email'), create_message))
    this.setState({
      create_message: '',
    })
  }

  handleSubmit = () => {
    const { create_message } = this.state

    if ( create_message ) {
      this.postData()
    }
  }

  handleKeyPress = (event) => {
    const { create_message} = this.state

    if ( event.key === 'Enter' ) {
      event.preventDefault()
      if ( create_message ) {
        this.postData()
      }
    }
  }

  handleChange = event => {
    this.setState({ create_message: event.target.value, });
  };

  handleMessage = () => {
    this.props.dispatch(createMessage())
    this.setState({ open: false })
  }

  renderMessages() {
    const { classes } = this.props
    const { chat } = this.state

    return (
      <div>
        {chat.map((data) => (
          <Paper className={classes.root} elevation={1} key={data.id}>
            <Typography variant="h5" component="h3">
              {data.name}
            </Typography>
            <Typography component="p">
              {data.message}
            </Typography>
          </Paper>
        ))}
      </div>
    )
  }

  render() {
    const { classes, user } = this.props
    const { create_message } = this.state
    
    return (
      <div>
        {user.get('email') ?
          <div>
            <form className={classes.container} noValidate autoComplete="off">
              <TextField
                fullWidth
                id="outlined-error"
                label="Enter Message"
                className={classes.textField}
                value={create_message}
                onChange={this.handleChange}
                margin="normal"
                variant="outlined"
                onKeyDown={this.handleKeyPress}
              />
            </form>
            {this.renderMessages()}
        </div>
          :
          <div>
            <Typography variant="h5" component="h3">
              Login to view chat
            </Typography>
          </div>
        }
      </div>
    )
  }
}

export default withStyles(styles)(connect(mapStateToProps)(Chat))
