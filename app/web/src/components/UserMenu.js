import React from 'react'
import { Link } from 'react-router-dom'
import apiHost from '../apiHost'
import { withStyles } from '@material-ui/core/styles'
import Immutable from 'immutable'
import { connect } from 'react-redux'
import { logoutUser } from '../actions'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import AccountCircle from '@material-ui/icons/AccountCircle'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import BugReportIcon from '@material-ui/icons/BugReport'
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount'
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications'
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew'

const styles = theme => ({
  popperClose: {
    pointerEvents: 'none',
  },
  menuList: {
    outline: 0,
  },
  link: {
    border: 'none',
    outline: 'none',
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
  avatar: {
    width: 30,
    height: 30,
  },
  text: {},
  icon: {},
})

const mapStateToProps = state => ({
  user: state.get('user'),
})

class UserMenu extends React.Component {
  state = {
    anchorEl: null,
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (!Immutable.is(this.props.user, nextProps.user) || this.state !== nextState)
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleLogOut = () => {
    this.props.dispatch(logoutUser())
    this.setState({ anchorEl: null })
  }

  render() {
    const { classes, user } = this.props
    const { anchorEl } = this.state
    const open = Boolean(anchorEl);
    const admin = user.get('access') ? user.get('access').toJS().includes('admin') : false

    return (
      <div>
        <IconButton
          aria-owns={open ? 'menu-list' : undefined}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="default"
        >
          {user.get('email') ? <Avatar alt={user.get('avatar')} src={`${apiHost}${user.get('avatar')}`} className={classes.avatar} /> : <AccountCircle />}
        </IconButton>
        {user.get('email') ?
          <Menu id="menu-list" 
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={this.handleClose}>
            {admin &&
              <div className={classes.menuList}>
                <Link to={`/admin/users`} className={classes.link}>
                  <MenuItem onClick={this.handleClose}>
                    <ListItemIcon className={classes.icon}><SupervisorAccountIcon /></ListItemIcon>
                    <ListItemText inset primary="Manage Users" />
                  </MenuItem>
                </Link>
                <Link to={`/admin/logs`} className={classes.link}>
                  <MenuItem onClick={this.handleClose}>
                    <ListItemIcon className={classes.icon}><BugReportIcon /></ListItemIcon>
                    <ListItemText inset primary="System Logs" />
                  </MenuItem>
                </Link>
              </div>
            }
            <Link to={`/profile/${user.get('name')}`} className={classes.link}>
              <MenuItem onClick={this.handleClose}>
                <ListItemIcon className={classes.icon}><AccountBoxIcon /></ListItemIcon>
                <ListItemText inset primary="Profile" />
              </MenuItem>
            </Link>
            <Link to='/settings' className={classes.link}>
              <MenuItem onClick={this.handleClose}>
                <ListItemIcon className={classes.icon}><SettingsApplicationsIcon /></ListItemIcon>
                <ListItemText inset primary="Settings" />
              </MenuItem>
            </Link>
            <MenuItem onClick={this.handleLogOut}>
              <ListItemIcon className={classes.icon}><PowerSettingsNewIcon /></ListItemIcon>
              <ListItemText inset primary="Logout" />
            </MenuItem>
          </Menu>
          :
          <Menu id="menu-list" 
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={this.handleClose}>
            <Link to='/login' className={classes.link}><MenuItem onClick={this.handleClose}>Login</MenuItem></Link>
            <Link to='/register' className={classes.link}><MenuItem onClick={this.handleClose}>Register</MenuItem></Link>
          </Menu>
        }
      </div>
    )
  }
}

export default withStyles(styles)(connect(mapStateToProps)(UserMenu))
