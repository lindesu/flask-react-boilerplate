import axios from 'axios'
import apiHost from './apiHost'
console.log(apiHost)

export const setUser = (data) => {
  return {
    type: 'SET_USER',
    data
  }
}

export const setProfile = (data) => {
  return {
    type: 'SET_PROFILE',
    data
  }
}

export const setUsers = (data) => {
  return {
    type: 'SET_USERS',
    data
  }
}

export const setLogs = (data) => {
  return {
    type: 'SET_LOGS',
    data
  }
}

export const setMessages = (data) => {
  return {
    type: 'SET_MESSAGES',
    data
  }
}

export const getUser = () => {
  return dispatch => {
    return axios.get(`${apiHost}/api/user/`, { withCredentials: true })
    .then(
      response => { dispatch(setUser(response.data)) },
      error => { dispatch(setUser({})) }
    )
  }
}

export const getProfile = (username) => {
  return dispatch => {
    return axios.get(`${apiHost}/api/user/profile/${username}`, { withCredentials: true })
    .then(
      response => { dispatch(setProfile(response.data)) },
      error => { dispatch(setProfile({})) }
    )
  }
}

export const registerUser = (email, name) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/auth/register`, { email, name }, { withCredentials: true })
    .then(
      error => { console.log(error) },
    )
  }
}

export const loginUser = (email, password) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/auth/login`, { email, password }, { withCredentials: true })
    .then(
      response => { dispatch(getUser()) },
    )
  }
}

export const updateUserInfo = (email, name) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/user/update`, { email, name }, { withCredentials: true })
    .then(
      response => { dispatch(getUser()) },
    )
  }
}

export const updateUserAvatar = (formData) => {
  return dispatch => {
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      },
      withCredentials: true
    }

    return axios.post(`${apiHost}/api/user/update/avatar`, formData, config)
    .then(
      response => { dispatch(getUser()) },
    )
  }
}

export const updateUserPassword = (old_password, new_password) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/user/update/password`, { old_password, new_password }, { withCredentials: true })
    .then(
      response => { dispatch(logoutUser()) },
    )
  }
}

export const logoutUser = () => {
  return dispatch => {
    return axios.get(`${apiHost}/api/auth/logout`, { withCredentials: true })
    .then(
      response => { dispatch(setUser({})) },
    )
  }
}

export const getLogs = () => {
  return dispatch => {
    return axios.get(`${apiHost}/api/admin/logs`, { withCredentials: true })
    .then(
      response => { dispatch(setLogs(response.data)) },
      error => { dispatch(setLogs({})) }
    )
  }
}

export const getUsers = () => {
  return dispatch => {
    return axios.get(`${apiHost}/api/admin/users`, { withCredentials: true })
    .then(
      response => { dispatch(setUsers(response.data)) },
      error => { dispatch(setUsers({})) }
    )
  }
}

export const getMessages = () => {
  return dispatch => {
    return axios.get(`${apiHost}/api/realtime`, { withCredentials: true })
    .then(
      response => { dispatch(setMessages(response.data)) },
      error => { console.log(error) }
    )
  }
}

export const createMessage = (name, message) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/realtime/create_message`, { name, message }, { withCredentials: true })
    .then(
      response => { console.log(response) },
      error => { console.log(error) }
    )
  }
}

export const adminCreateUser = (name, email, test) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/admin/user/create`, { name, email, test }, { withCredentials: true })
    .then(
      response => { dispatch(getUsers()) },
    )
  }
}

export const adminUpdateUserInfo = (id, name, email) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/admin/user/update`, { id, name, email }, { withCredentials: true })
    .then(
      response => { dispatch(getUsers()) },
    )
  }
}

export const adminUpdateUserAccess = (id, access) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/admin/user/access`, { id, access }, { withCredentials: true })
    .then(
      response => { dispatch(getUsers()) },
    )
  }
}

export const adminResetPassword = (id) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/admin/user/resetpw`, { id }, { withCredentials: true })
    .then(
      response => { dispatch(getUsers()) },
    )
  }
}

export const adminDisableUser = (id) => {
  return dispatch => {
    return axios.post(`${apiHost}/api/admin/user/disable`, { id }, { withCredentials: true })
    .then(
      response => { dispatch(getUsers()) },
    )
  }
}
